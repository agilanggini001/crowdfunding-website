<?php

namespace App;
use App\Traints\UseUuid;

use Illuminate\Database\Eloquent\Model;

class otp_code extends Model
{
    use UseUuid;
    protected $fillable = ['otp','user_id','valid_until'];

    public function user()
    {
        return $this->BelongsTo('App\User', 'user _id');
    }
}
