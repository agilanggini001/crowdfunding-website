<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traints\UseUuid;

class Role extends Model
{
    use UseUuid;
    protected $fillable = ['name'];

    public function users()
    {
        return $this->hasMany('App\User', 'role_id');
    }
}
