<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        $user = auth()->user();

        if($user->role_id == '2fa8313e-d2bb-48d7-8be0-634926bf74ab' ){
            return $next($request);
        }

        return response()->json([
            'message' => 'Anda bukan Admin'
        ]);
    }
}
