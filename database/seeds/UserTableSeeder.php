<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => Str::random(10),
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('admin'),
            'role_id' => '2fa8313e-d2bb-48d7-8be0-634926bf74ab',
        ]);

        User::create([
            'id' => Str::random(10),
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('admin'),
            'role_id' => '2fa8313e-d2bb-48d7-8be0-634926bf74ab',
        ]);

        User::create([
            'id' => Str::random(10),
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('user'),
            'role_id' => '9ab0c276-b827-427a-a4a0-6be4ff36670a',
        ]);

        User::create([
            'id' => Str::random(10),
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'role_id' => '9ab0c276-b827-427a-a4a0-6be4ff36670a',
        ]);

        User::create([
            'id' => Str::random(10),
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('user'),
            'role_id' => '9ab0c276-b827-427a-a4a0-6be4ff36670a',
        ]);
    }
}
